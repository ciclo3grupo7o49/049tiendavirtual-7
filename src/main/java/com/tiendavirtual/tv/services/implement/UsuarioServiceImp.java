package com.tiendavirtual.tv.services.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.tiendavirtual.tv.dao.UsuarioDao;
import com.tiendavirtual.tv.modelos.Usuario;
import com.tiendavirtual.tv.services.UsuarioService;

@Service
public class UsuarioServiceImp implements UsuarioService {

    @Autowired
    private UsuarioDao usuarioDao;

    @Override
    @Transactional(readOnly = false)
    public Usuario save(Usuario cliente) {

        return usuarioDao.save(cliente);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        usuarioDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public Usuario findById(Integer id) {

        return usuarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Usuario> findAll() {

        return (List<Usuario>) usuarioDao.findAll();
    }

    @Override
    public Usuario login(String username, String password) {
        
        return null;
    }

}
