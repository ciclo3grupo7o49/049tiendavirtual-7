package com.tiendavirtual.tv.services.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tiendavirtual.tv.dao.TransaccionDao;
import com.tiendavirtual.tv.modelos.Transaccion;
import com.tiendavirtual.tv.services.TransaccionService;

@Service
public class TransaccionServiceImp implements TransaccionService {

    @Autowired
    private TransaccionDao transaccionDao;

    @Override
    @Transactional(readOnly = false)
    public Transaccion save(Transaccion transaccion) {

        return transaccionDao.save(transaccion);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        transaccionDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public Transaccion findById(Integer id) {

        return transaccionDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Transaccion> findAll() {

        return (List<Transaccion>) transaccionDao.findAll();
    }

}
