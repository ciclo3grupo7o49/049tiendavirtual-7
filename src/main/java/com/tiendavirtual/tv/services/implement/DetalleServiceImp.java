package com.tiendavirtual.tv.services.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tiendavirtual.tv.dao.DetalleDao;
import com.tiendavirtual.tv.modelos.Detalle;
import com.tiendavirtual.tv.services.DetalleService;


@Service
public class DetalleServiceImp implements DetalleService {
    @Autowired
    private DetalleDao detalleDao;

    @Override
    @Transactional(readOnly = false)
    public Detalle save(Detalle detalles) {

        return detalleDao.save(detalles);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        detalleDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public Detalle findById(Integer id) {

        return detalleDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Detalle> findAll() {

        return (List<Detalle>) detalleDao.findAll();
    }

}
