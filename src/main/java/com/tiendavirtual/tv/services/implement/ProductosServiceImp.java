package com.tiendavirtual.tv.services.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tiendavirtual.tv.dao.ProductosDao;
import com.tiendavirtual.tv.modelos.Productos;
import com.tiendavirtual.tv.services.ProductosService;

@Service
public class ProductosServiceImp implements ProductosService {
    @Autowired
    private ProductosDao productosDao;

    @Override
    @Transactional(readOnly = false)
    public Productos save (Productos productos) {
        return productosDao.save(productos);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        productosDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public Productos findById(Integer id) {

        return productosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Productos> findAll() {

        return (List<Productos>) productosDao.findAll();
    }


}
