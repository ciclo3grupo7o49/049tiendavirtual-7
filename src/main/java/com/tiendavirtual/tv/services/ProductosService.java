package com.tiendavirtual.tv.services;

import java.util.List;

import com.tiendavirtual.tv.modelos.Productos;

public interface ProductosService {
    public Productos save(Productos productos);

    public void delete(Integer id);

    public Productos findById(Integer id);

    public List<Productos> findAll();

}
