package com.tiendavirtual.tv.services;


import com.tiendavirtual.tv.modelos.Transaccion;

import java.util.List;

public interface TransaccionService {
    public Transaccion save(Transaccion transaccion);

    public void delete(Integer id);

    public Transaccion findById(Integer id);

    public List<Transaccion> findAll();

}
