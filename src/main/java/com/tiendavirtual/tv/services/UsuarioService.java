package com.tiendavirtual.tv.services;

import com.tiendavirtual.tv.modelos.Usuario;
import java.util.List;

public interface UsuarioService {
    public Usuario save(Usuario usuario);

    public void delete(Integer id);

    public Usuario findById(Integer id);

    public List<Usuario> findAll();

    public Usuario login(String username, String password);
}