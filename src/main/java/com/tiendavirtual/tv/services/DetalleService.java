package com.tiendavirtual.tv.services;

import java.util.List;

import com.tiendavirtual.tv.modelos.Detalle;


public interface DetalleService {

    public Detalle save(Detalle detalle);

    public void delete(Integer id);

    public Detalle findById(Integer id);

    public List<Detalle> findAll();

}
