package com.tiendavirtual.tv.dao;

import org.springframework.data.repository.CrudRepository;

import com.tiendavirtual.tv.modelos.Productos;





public interface ProductosDao extends CrudRepository<Productos,Integer> {

  
}
