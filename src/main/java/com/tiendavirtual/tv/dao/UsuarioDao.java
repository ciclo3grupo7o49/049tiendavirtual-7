package com.tiendavirtual.tv.dao;


import org.springframework.data.repository.CrudRepository;

import com.tiendavirtual.tv.modelos.Usuario;



public interface UsuarioDao  extends CrudRepository<Usuario,Integer>{
    /**@Transactional (readOnly=true)
    /**@Query(value="SELECT * FROM usuario WHERE activo = 1 and username =:username and password =:password")
    /**public Usuario login(@param("username") String username @Param ("password") String password);/* */
}
