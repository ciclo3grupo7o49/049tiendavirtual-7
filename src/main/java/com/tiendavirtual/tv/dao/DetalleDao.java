package com.tiendavirtual.tv.dao;

import org.springframework.data.repository.CrudRepository;

import com.tiendavirtual.tv.modelos.Detalle;

public interface DetalleDao extends CrudRepository<Detalle,Integer> {
    
}
