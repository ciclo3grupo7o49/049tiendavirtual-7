package com.tiendavirtual.tv.dao;

import org.springframework.data.repository.CrudRepository;

import com.tiendavirtual.tv.modelos.Transaccion;

public interface TransaccionDao extends CrudRepository <Transaccion,Integer>{
    
}
