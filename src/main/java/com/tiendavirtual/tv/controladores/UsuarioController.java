package com.tiendavirtual.tv.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tiendavirtual.tv.modelos.Usuario;
import com.tiendavirtual.tv.services.UsuarioService;

@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")

public class UsuarioController{
    @Autowired
    private UsuarioService usuarioservice;

    @PostMapping(value="/")
    public ResponseEntity<Usuario> agregar (@RequestBody Usuario usuario){
        Usuario obj = usuarioservice.save(usuario);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id){
        Usuario obj = usuarioservice.findById(id);          
            if(obj!=null){  
                usuarioservice.delete(id);          
            }else{                  
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);         
            }         
            return new ResponseEntity<>(obj, HttpStatus.OK);      
        }

        @PutMapping(value="/")
        public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario){
        Usuario obj = usuarioservice.findById(usuario.getIdProductos()); 
            if(obj!=null) { 
            obj.setUsername(usuario.getUsername());
            obj.setPassword(usuario.getPassword());
            obj.setEmail(usuario.getEmail());            
            usuarioservice.save(obj);          
        } else{
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);         
            }          
            return new ResponseEntity<>(obj, HttpStatus.OK);      
        }

        @GetMapping("/lista")      
        public List<Usuario> consultarTodo(){
           return usuarioservice.findAll();      
        }           
        
        @GetMapping("/lista/{id}")      
        public Usuario consultaPorId(@PathVariable Integer id){
            return usuarioservice.findById(id);      
        }

        @GetMapping("/login")      
        public Usuario consultaPorNombre(@RequestParam("username") String userName, @RequestParam("password") String password){ 
                     return usuarioservice.login(userName, password);      
                    }
}