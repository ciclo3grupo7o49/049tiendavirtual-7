package com.tiendavirtual.tv.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiendavirtual.tv.modelos.Productos;
import com.tiendavirtual.tv.services.ProductosService;



@RestController
@CrossOrigin("*")
@RequestMapping("/producto")

public class ProductosController{
    @Autowired
    private ProductosService productosservice;

    @PostMapping(value="/")
    public ResponseEntity<Productos> agregar (@RequestBody Productos productos){
        Productos obj = productosservice.save(productos);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Productos> eliminar(@PathVariable Integer id){
        Productos obj = productosservice.findById(id);          
            if(obj!=null){  
                productosservice.delete(id);          
            }else{                  
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);         
            }         
            return new ResponseEntity<>(obj, HttpStatus.OK);      
        }

       
        @PutMapping(value="/")
        public ResponseEntity<Productos> editar(@RequestBody Productos productos){
        Productos obj = productosservice.findById(productos.getIdProducto()); 
            if(obj!=null) { 
            obj.setCantidad(productos.getCantidad());                  
            obj.setNombreProducto(productos.getNombreProducto());                  
            obj.setValorCompra(productos.getValorCompra());                  
            obj.setValorVenta(productos.getValorVenta());              
            productosservice.save(obj);          
        } else{
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);         
            }          
            return new ResponseEntity<>(obj, HttpStatus.OK);      
        }

        @GetMapping("/lista")      
        public List<Productos> consultarTodo(){
           return productosservice.findAll();      
        }           
        
        @GetMapping("/lista/{id}")      
        public Productos consultaPorId(@PathVariable Integer id){
            return productosservice.findById(id);      
        }

}