package com.tiendavirtual.tv.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiendavirtual.tv.modelos.Transaccion;
import com.tiendavirtual.tv.services.TransaccionService;

@RestController
@CrossOrigin("*")
@RequestMapping("/transaccion")
public class TransaccionController {
    @Autowired
    private TransaccionService transaccionservice;

    @PostMapping(value="/")
    public ResponseEntity<Transaccion> agregar (@RequestBody Transaccion transaccion){
        Transaccion obj = transaccionservice.save(transaccion);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Transaccion> eliminar(@PathVariable Integer id){
        Transaccion obj = transaccionservice.findById(id);          
            if(obj!=null){  
                transaccionservice.delete(id);          
            }else{                  
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);         
            }         
            return new ResponseEntity<>(obj, HttpStatus.OK);      
        }

        /**
         * @param productos
         * @return
         */
        @PutMapping(value="/")
        public ResponseEntity<Transaccion> editar(@RequestBody Transaccion transaccion){
            Transaccion obj = transaccionservice.findById(transaccion.getIdTransaccion()); 
            if(obj!=null) { 
            obj.setTipoTransaccion(transaccion.getTipoTransaccion());              
            obj.setFechaTransaccion(transaccion.getFechaTransaccion());             
            obj.setVendedor(transaccion.getVendedor());    
            obj.setComprador(transaccion.getComprador()); 
            obj.setTotal(transaccion.getTotal());                      
            transaccionservice.save(obj);          
        } else{
             return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);         
            }          
            return new ResponseEntity<>(obj, HttpStatus.OK);      
        }

        @GetMapping("/lista")      
        public List<Transaccion> consultarTodo(){
           return transaccionservice.findAll();      
        }           
        
        @GetMapping("/lista/{id}")      
        public Transaccion consultaPorId(@PathVariable Integer id){
            return transaccionservice.findById(id);      
        }
}
