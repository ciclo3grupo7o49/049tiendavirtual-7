package com.tiendavirtual.tv.modelos;

import java.io.Serializable;



import jakarta.persistence.Table;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;


@Entity
@Table(name = "productos")
public class Productos implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idProducto;

 

    @Column(name = "nombreproducto", nullable = false)
    private String nombreProducto;

    @Column(name = "valorcompra", nullable = false)
    private Double valorCompra;

    @Column(name = "valorventa", nullable = false)
    private Integer valorVenta;

    @Column(name = "cantidad", nullable = false)
    private Integer cantidad;



    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public Double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(Double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public Integer getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(Integer valorVenta) {
        this.valorVenta = valorVenta;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Productos(String nombreProducto, Double valorCompra, Integer valorVenta, Integer cantidad) {
        this.nombreProducto = nombreProducto;
        this.valorCompra = valorCompra;
        this.valorVenta = valorVenta;
        this.cantidad = cantidad;
    }

    



}
