package com.tiendavirtual.tv.modelos;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "detalle")

public class Detalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="iddetalle")
    private Integer IdDetalle;
    
    @ManyToOne
    @JoinColumn(name = "idusuario")
    private Usuario usuario;
    
    @ManyToOne
    @JoinColumn(name = "idproducto")
    private Productos productos;

    @ManyToOne
    @JoinColumn(name = "idtransaccion")
    private Transaccion transaccion;
 
    @Column(name = "valordetalle")
    private Double valorDetalle;

    @Column(name = "cantidaddetalle")
    private Double cantidadDetalle;

    @Column(name = "totalddetalle")
    private Double totalDetalle;

    public Integer getIdDetalle() {
        return IdDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        IdDetalle = idDetalle;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Productos getProductos() {
        return productos;
    }

    public void setProductos(Productos productos) {
        this.productos = productos;
    }

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    public Double getValorDetalle() {
        return valorDetalle;
    }

    public void setValorDetalle(Double valorDetalle) {
        this.valorDetalle = valorDetalle;
    }

    public Double getCantidadDetalle() {
        return cantidadDetalle;
    }

    public void setCantidadDetalle(Double cantidadDetalle) {
        this.cantidadDetalle = cantidadDetalle;
    }

    public Double getTotalDetalle() {
        return totalDetalle;
    }

    public void setTotalDetalle(Double totalDetalle) {
        this.totalDetalle = totalDetalle;
    }

    
}
